const loadTemplate = require('./loadTemplate');
const {
    putCurrentDate
} = require('./util');

module.exports = {
    loadTemplate,
    putCurrentDate
};