const putCurrentDate = (str) => {
    const today = new Date();

    return str.replace(
        /(date[a-z ]*:).*/i,
        `$1 ${today.getUTCFullYear()}-${today.getUTCMonth() + 1}-${today.getUTCDate()}`
    );
};

module.exports = {
    putCurrentDate
};