const path = require('path');
const promisify = require('util').promisify;

const fsReadFile = promisify(require('fs').readFile);
const fsStat = promisify(require('fs').stat);
const exec = promisify(require('child_process').exec);

const appName = 'sia';

const fileExists = async (filepath) => {
    try {
        await fsStat(filepath);
    }
    catch (err) {
        return false;
    }

    return true;
}

const getSystemConfigPath = async (filename) => {
    let result;
    try {
        result = await exec('npm root -g');
    }
    catch (err) {
        return '';
    }

    console.log(path.join(result.stdout.trim(), appName, filename));

    return path.join(result.stdout.trim(), appName, filename);
}

const getAvailableConfigPath = async (filename) => {
    const localPath = path.join('./', filename);
    if (await fileExists(localPath)) {
        return localPath;
    }

    let systemPath = await getSystemConfigPath(filename);
    if (await !fileExists(systemPath)) {
        throw Error('Unable to locate configuration file');
    }
    return systemPath;
}

module.exports = async (filename = '.student-info.txt') => {
    const configPath = await getAvailableConfigPath(filename);

    let data;
    try {
        data = await fsReadFile(configPath);
    }
    catch (err) {
        throw Error(`Unable to read ${configPath}`);
    }

    return data.toString();
};