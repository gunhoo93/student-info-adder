#!/usr/bin/env node
'use strict';
const program = require('commander');
const prependFile = require('prepend-file');
const {
    loadTemplate,
    putCurrentDate
} = require('../lib');

const prependTemplate = async (files) => {
    let template;
    try {
        template = await loadTemplate();
    } catch (err) {
        throw err;
    }
    template = putCurrentDate(template);

    files.forEach(file => {
        prependFile(file, template, err => {
            if (err) {
                console.log(err);
            }
            console.log(`Student information added to ${file}`);
        });
    });
}

program
    .command('prepend [files...]')
    .action(files => prependTemplate(files));

program.parse(process.argv);
