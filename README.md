#student-info-adder
prepends a student info header used in OOP345 class to files using a template.

##Motivation
It becomes tedious to add student info header on every file that you work on.
Once you've added student info template, you can automate the tedious copy and paste based insertion.

##Feature
* create files with student info header from a template.
* inserts date current date for date field.

##Installation
1. clone the repository and navigate to it
2. `npm intall -g`
3. add .student-info.txt file to globally installed package directory
4. `sia prepend file1 file2 file3 ...` will add student info at the top of each file

##Template example
For now, the app only transforms date field with the date of creation. Anything else is simple text substitution.
```
// Name: your-name
// Student ID: your-id
// Email: your-email
// Date: 
```
Having a field with `/date.*:.*/` pattern will trigger auto date insertion. 

